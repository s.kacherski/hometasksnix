import java.util.Arrays;
import java.util.stream.Collectors;

public class CSVAggregator implements Aggregator {
    @Override
    public Object aggregate(Object[] items) {
        return Arrays.stream(items).map(String::valueOf).collect(Collectors.joining(","));
    }
}
