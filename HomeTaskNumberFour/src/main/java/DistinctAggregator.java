import java.util.Arrays;

public class DistinctAggregator implements Aggregator {
    @Override
    public Long aggregate(Object[] items) {
        return Arrays.stream(items).distinct().count();
    }
}
