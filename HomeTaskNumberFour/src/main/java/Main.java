import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        DistinctAggregator da = new DistinctAggregator();
        AvgAggregator aa = new AvgAggregator();
        MaxAggregator ma = new MaxAggregator();
        CSVAggregator cs = new CSVAggregator();
        Integer[] ints = new Integer[]{1, 2, 3, 3, 4};
        String[] list = new String[]{"a", "a", "b"};

        System.out.println(da.aggregate(list));
        System.out.println(da.aggregate(ints));

        //      System.out.println(aa.aggregate(list));
        System.out.println(aa.aggregate(ints));

        System.out.println(ma.aggregate(list));
        System.out.println(ma.aggregate(ints));

        System.out.println(cs.aggregate(list));
        System.out.println(cs.aggregate(ints));

    }
}
