import java.util.Arrays;
import java.util.OptionalDouble;

public class AvgAggregator implements Aggregator<OptionalDouble, Number> {

    @Override
    public OptionalDouble aggregate(Number[] items) {

        return Arrays.stream(items).mapToDouble(Number::doubleValue).average();
    }
}
