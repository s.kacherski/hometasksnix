import java.util.Arrays;
import java.util.Comparator;

public class MaxAggregator<T extends Comparable<? super T>> implements Aggregator<T, T> {


    @Override
    public T aggregate(T[] items) {
        return Arrays.stream(items).max(Comparator.naturalOrder()).get();
    }
}
