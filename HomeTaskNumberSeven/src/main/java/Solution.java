import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {

    private static List<File> allFilesInDirectory(String path) {
        List<File> result = new ArrayList<>();
        File[] files = new File(path).listFiles();

        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    result.add(file);
                } else if (file.isDirectory()) {
                    allFilesInDirectory(file.getAbsolutePath());
                }
            }
        }
        return result;
    }

    public void solution(String path, String regex) {
        Pattern pattern = Pattern.compile(regex);
        String line;
        Matcher matcher;
        BufferedReader br;
        List<File> files = allFilesInDirectory(path);
        for (File f : files) {
            System.out.println(f.getAbsolutePath());
            try {
                br = new BufferedReader(new FileReader(path));
                while ((line = br.readLine()) != null) {
                    matcher = pattern.matcher(line);
                    if (matcher.find()) {

                        System.out.println(line.replaceAll(regex, "[$0]"));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    public static void main(String[] args) {
        Solution sol = new Solution();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Give path to file or directory");
        String path = scanner.nextLine();
        System.out.println("Give regex to match");
        String regex = scanner.nextLine();
        sol.solution(path, regex);


    }
}
