import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ValidateInput {

    public boolean validateInput(String path, int startX, int startY, int endX, int endY) {

        File pathValidate = new File(path);

        if (!pathValidate.isFile()) {
            return false;
        }
        if (startX < 0 || startY < 0 || endX < 0 || endY < 0) {
            return false;
        }
        if (startX == endX && startY == endY) {
            return false;
        }

        return true;
    }


}
