import java.awt.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        System.out.println("Give path to file or directory");
        String path = scanner.nextLine();
        System.out.println("Enter start coordinates");
        System.out.println("Enter X");

        int startX = scanner.nextInt();
        System.out.println("Enter Y");
        int startY = scanner.nextInt();
        System.out.println("Enter finish coordinates");
        System.out.println("Enter X");
        int endX = scanner.nextInt();
        System.out.println("Enter Y");
        int endY = scanner.nextInt();
        ValidateInput validateInput = new ValidateInput();
        if (!validateInput.validateInput(path, startX, startY, endX, endY)) {
            throw new IllegalArgumentException();
        }
        GetLabyrinth getLabyrinth = new GetLabyrinth(path);

        Solution solution = new Solution(getLabyrinth.buildLab(), new Point(startX, startY), new Point(endX, endY));
        if (!(solution.computeDist() < 0)) {
            Output output = new Output(getLabyrinth.buildLab(), solution.getPath());
            output.printLab();
        } else {
            System.out.println("Решений нет");
        }
    }
}





