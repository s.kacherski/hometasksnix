import java.io.*;

public class GetLabyrinth {
    private final String path;
    private static char[][] lab;


    public GetLabyrinth(String path) {
        this.path = path;
    }

    public char[][] buildLab() {
        String str;
        int i = 0;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            int height = 0;
            int width = 0;
            while ((str = bufferedReader.readLine()) != null) {
                width = str.length();
                height++;
            }
            lab = new char[width][height];
            while ((str = bufferedReader.readLine()) != null) {

                for (int j = 0; j < str.length(); i++) {
                    lab[i][j] = str.trim().charAt(j);
                    i++;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return lab;

    }
}


