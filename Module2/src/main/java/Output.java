import java.awt.*;
import java.util.ArrayList;

public class Output {
    public char[][] lab;
    public ArrayList<Point> points;

    public Output(char[][] lab, ArrayList<Point> points) {
        this.lab = lab;
        this.points = points;
    }

    public void printLab() {
        for (Point point : points) {
            lab[(int) point.getX()][(int) point.getY()] = '#';

        }
        for (char[] chars : lab) {
            System.out.println(chars);
        }
    }


}
