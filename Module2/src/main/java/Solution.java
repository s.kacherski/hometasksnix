import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * This class describes a wave algorithm that computes the minimal
 * path from some start point to some finish point.
 *
 * @author Mikhail Ruchkin
 */
public class Solution {
    private final char[][] table;
    private final int rows;
    private final int cols;
    private final Point startPoint;
    private final Point finishPoint;
    private LinkedHashMap<String, Point> previousPoints;


    public Solution(char[][] table, Point startPoint, Point finishPoint) {
        this.table = table;
        this.rows = table.length;
        this.cols = table[0].length;
        this.startPoint = startPoint;
        this.finishPoint = finishPoint;
    }

    private boolean isOutOfField(Point p) {
        return (p.getX() < 0 || p.getX() >= cols || p.getY() < 0 || p.getY() >= rows);
    }

    public int computeDist() {
        int[][] dist = new int[rows][cols];
        int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
        ArrayList<Point> oldFront = new ArrayList<>();
        ArrayList<Point> newFront = new ArrayList<>();
        int currentDist = 0;
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++) {
                dist[i][j] = !(table[i][j] == '+') ? -1 : Integer.MAX_VALUE;
            }
        oldFront.add(startPoint);
        dist[(int) startPoint.getX()][(int) startPoint.getY()] = 0;
        previousPoints = new LinkedHashMap<>();
        while (true) {
            currentDist++;
            for (Point p : oldFront) {
                for (int[] dir : directions) {
                    Point newPoint = new Point((int) (p.getX() + dir[0]), (int) (p.getY() + dir[1]));
                    if (isOutOfField(newPoint)) continue;
                    if (dist[(int) newPoint.getX()][(int) newPoint.getY()] != -1) continue;
                    dist[(int) newPoint.getX()][(int) newPoint.getY()] = currentDist;
                    previousPoints.put(newPoint.toString(), p);
                    if (newPoint.equals(finishPoint)) return currentDist;
                    newFront.add(newPoint);
                }
            }
            if (newFront.isEmpty()) return -1;
            oldFront = new ArrayList<>(newFront);
            newFront.clear();
        }
    }


    public ArrayList<Point> getPath() {
        if (!previousPoints.containsKey(finishPoint.toString()))
            return null;
        Point lastPoint = finishPoint;
        ArrayList<Point> path = new ArrayList<>();
        while (!lastPoint.equals(startPoint)) {
            path.add(lastPoint);
            lastPoint = previousPoints.get(lastPoint.toString());
        }
        path.add(startPoint);
        return path;
    }
}