import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Solution {


    public int stringOfDigits(String[] s) {
        return Integer.parseInt(Arrays.toString(s)
                .chars()
                .filter(Character::isDigit)
                .mapToObj(Character::toString)
                .collect(Collectors.joining("")));
    }

    public Optional<BigDecimal> biggestNumber(List<BigDecimal> list) {
        return list.stream()
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()))
                .entrySet().stream().max(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey);
    }

    public Map<LocalDate, List<LocalTime>> groupByDateTime(List<LocalDateTime> list) {
        return list.stream()
                .collect(Collectors.groupingBy(LocalDateTime::toLocalDate, Collectors.mapping(LocalDateTime::toLocalTime, Collectors.toList())))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }


    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.stringOfDigits(new String[]{"string 1text", "2string 3text", "45"}));
        BigDecimal[] bigDecimals = new BigDecimal[]
                {new BigDecimal("1"), new BigDecimal("2"), new BigDecimal("3"), new BigDecimal("3")};

        System.out.println(solution.biggestNumber(List.of(bigDecimals)));


    }
}
