public abstract class Substance {

    protected double temperature = 20;

    double temperatureSOLID;
    double temperatureLIQUID;

    public Substance(double temperatureSOLID, double temperatureLIQUID) {
        this.temperatureSOLID = temperatureSOLID;
        this.temperatureLIQUID = temperatureLIQUID;
    }

    void heatUp(double t) {
        temperature = temperature + t;

        if (temperature <= temperatureSOLID) System.out.println(State.SOLID + " " + temperature);
        else if (temperature < temperatureLIQUID) System.out.println(State.LIQUID + " " + temperature);
        else System.out.println(State.GASEOUS + " " + temperature);
    }



}
