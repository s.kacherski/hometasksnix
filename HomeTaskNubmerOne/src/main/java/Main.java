import java.util.Scanner;

public class Main {
    private static Substance workWithIt;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Выбирите одно из веществ:  Water, Iron, Oxygen");
        String a = sc.nextLine();

        switch (a) {
            case "Water" -> workWithIt = new Water(0, 100);

            case "Iron" -> workWithIt = new Iron(1538, 2735);

            case "Oxygen" -> workWithIt = new Oxygen(-218.35, -182.96);


        }
        do {
            System.out.println("Введите значение изменения температуры");
            int b = sc.nextInt();
            workWithIt.heatUp(b);
            if (b == 0) break;
        } while (true);
    }
}
