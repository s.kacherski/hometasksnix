import java.util.ArrayList;

public class Group {

    private ArrayList<Student> students;


    public static void taskSolve(ArrayList<Student> students) {
        for (Student student : students) {
            if (student instanceof ContractStudent) {
                System.out.println("Имя = " + student.getName() + " Цена контракта = " + ((ContractStudent) student).getContractPrice());
            }

        }
    }


    public static void main(String[] args) {
        Group group = new Group();

        group.students = new ArrayList<>();

        Student student1 = new Student("Вася", 22);
        Student student2 = new Student("Вася2", 23);
        Student student3 = new Student("Вася3", 24);
        Student student4 = new ContractStudent("Вася4", 25, 1000);
        Student student5 = new ContractStudent("Вася5", 26, 2000);
        Student student6 = new ContractStudent("Вася6", 27, 3000);
        Student student7 = new ContractStudent("Вася7", 28, 4000);
        group.students.add(student1);
        group.students.add(student2);
        group.students.add(student3);
        group.students.add(student4);
        group.students.add(student5);
        group.students.add(student6);

        taskSolve(group.students);

    }
}
