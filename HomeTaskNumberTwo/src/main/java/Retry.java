import java.util.Scanner;

public class Retry {

    public void doWhileSuccessOrReachAttempts(int startDelay, int maxRetries, Block block) throws Exception {

        int count = 0;
        while (true) {
            try {

                block.run();
                break;

            } catch (Exception e) {
                System.out.println("Something wrong, try again");
                count++;
                if (count == maxRetries) {
                    System.out.println("Sorry, to many attempts, good bye");
                    throw e;

                }
                try {
                    Thread.sleep(startDelay);
                } catch (RuntimeException ex) {
                    ex.printStackTrace();
                }
                startDelay = startDelay * (count + 1);
            }

        }
    }


    public static void main(String[] args) {
        Retry retry = new Retry();
        try {
            retry.doWhileSuccessOrReachAttempts(200, 3, () -> {
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter number");
                int a = sc.nextInt();
                System.out.println(5 / a);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
