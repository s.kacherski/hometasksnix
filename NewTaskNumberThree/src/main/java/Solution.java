class Solution {

    public int maxArea(int[] height) {
        int rez = 0;
        int right = height.length - 1;
        for (int left = 0; left < right; ) {
            rez = Math.max(rez, (right - left) * Math.min(height[right], height[left]));

            if (height[left] <= height[right]) {
                left++;
            } else {
                right--;
            }
        }
        return rez;
    }


    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] height = new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7};
        System.out.println(solution.maxArea(height));


    }

}
