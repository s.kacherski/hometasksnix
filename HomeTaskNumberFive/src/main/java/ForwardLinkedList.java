import java.util.AbstractList;

public class ForwardLinkedList<T> extends AbstractList<T> {

    public int size;
    public Node<T> head;

    @Override
    public Node get(int index) {
        if (index > size - 1) {
            return null;
        }
        Node temp = head;
        for (int k = 0; k < index; k++) {
            temp = temp.getNext();
        }
        return temp;
    }

    @Override
    public boolean add(Object o) {
        {
            Node node = new Node<T>((T) o, null);
            if (head == null) {
                head = node;
            } else {
                Node temp = head;
                while (temp.getNext() != null) {
                    temp = temp.getNext();

                }
                temp.addNodeAfter(o);
                size++;
            }
        }

        return true;
    }

    @Override
    public int size() {
        return size;
    }


    public void removeByIndex(int index) {
        if (index < 0) {
            index = 0;
        }
        if (index >= size) {
            index = size - 1;
        }
        if (index == 0) {
            head = head.getNext();
        } else {
            Node temp = head;
            for (int i = 1; i < index; i += 1) {
                temp = temp.getNext();
            }
            temp.setNext(temp.getNext().getNext());
        }
        size--;
    }

    @Override
    public int indexOf(Object o) {
        int i = 0;
        Node node = new Node(o, null);
        if (head == null)
            return -1;
        if (head.equals(node))
            return 0;
        Node temp = head;
        while (temp.getNext() != null) {
            i++;
            if (temp.getNext().equals(node)) {
                return i;
            }
            temp = temp.getNext();
        }

        return -1;

    }
}

class Node<T> {
    private T data;

    private Node next;

    public Node(T data, Node next) {
        super();
        this.data = data;
        this.next = next;
    }

    protected Object getData() {
        return data;
    }

    protected void setData(T data) {
        this.data = data;
    }


    public void addNodeAfter(T t) {
        Node node = this.next;
        if (node == null) {
            this.next = new Node<T>(t, null);
        } else {
            this.next = new Node<T>(t, node);
        }
    }

    public void removeNodeAfter() {
        if (next != null) {
            next = next.next;
        }
    }

    protected Node getNext() {
        return next;
    }

    protected void setNext(Node next) {
        this.next = next;
    }


}
